db.fruits.aggregate([
    {$count: "fruitCount"}
])


db.fruits.aggregate([
    {$count: "onSale"}
])

db.fruits.aggregate([
    {$match: {"stock": {$gt: 20}}},
    {$count: "stock"}
])

db.fruits.aggregate([
    {$match: {"onSale": true}},
    {
        $group: {
            _id: "$id",
            avg_price: {$avg: "$price"}
        }
    }
])

db.fruits.aggregate([
    {$match: {"onSale": true}},
    {
        $group: {
            _id: "$id",
            max_price: {$max: "$price"}
        }
    }
])

db.fruits.aggregate([
    {$match: {"onSale": true}},
    {
        $group: {
            _id: "$id",
            min_price: {$min: "$price"}
        }
    }
])